# Github Roilan
https://github.com/Roilan/react-server-boilerplate

# Article Roilan
https://medium.com/front-end-weekly/server-side-rendering-with-react-and-express-382591bfc77c

# How to run
```
# Install All Package
$ npm i
# or
$ yarn

# Build
$ npm run build
# or
$ yarn build

# Start
$ npm start
#or
$ yarn start
```
