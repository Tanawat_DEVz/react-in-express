import React, { Component, PropTypes } from 'react';

export default class App extends Component {
  render() {
    const { isMobile, name, example } = this.props;

    return (
      <div>
        <h1>hello world {isMobile ? 'mobile' : 'desktop'} {name} {example}</h1>
      </div>
    );
  }
}

App.propTypes = {
  isMobile: PropTypes.bool.isRequired
};