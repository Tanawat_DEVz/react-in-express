import express from 'express';
import React from 'react';
import { renderToString } from 'react-dom/server';
import App from './app';
import template from './template';
import bodyParser from 'body-parser'

const server = express();

server.use('/assets', express.static('assets'));
server.use(bodyParser.json())

server.get('/:name', (req, res) => {
  const isMobile = true;
  const initialState = { isMobile, name: req.params.name };
  const appString = renderToString(<App {...initialState} />);

  res.send(template({
    body: appString,
    title: 'Hello World from the server',
    initialState: JSON.stringify(initialState)
  }));
});

server.post('/post/:name', (req, res) => {
    const isMobile = true;
    const initialState = { isMobile, name: req.params.name, example: req.body.example };
    const appString = renderToString(<App {...initialState} />);
  
    res.send(template({
      body: appString,
      title: 'Hello World from the server',
      initialState: JSON.stringify(initialState)
    }));
  });

server.listen(8080);
console.log('listening');